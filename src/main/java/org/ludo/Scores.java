package org.ludo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Scanner;

import static org.ludo.Fonctions.clearScreen;
import static org.ludo.Main.*;

public class Scores {
    // Chemin du fichier scores.json
    private static final String JSON = "./scores.json";
    public static void affichageDernieresEntrees() {
        String jsonContent = null;
        try {
            jsonContent = Files.readString(Paths.get(JSON));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // Créez un objet Gson
        Gson gson = new Gson();
        // Analysez le contenu JSON en un objet Java
        JsonObject jsonObject = gson.fromJson(jsonContent, JsonObject.class);
        // Accédez aux données du JSON
        JsonArray scoresArray = jsonObject.getAsJsonArray("scores");
        int totalEntries = scoresArray.size();
        int startIndex = Math.max(0, totalEntries - 10);
        for (int i = startIndex; i < totalEntries; i++) {
            JsonObject scoreObject = scoresArray.get(i).getAsJsonObject();
            String pseudo = scoreObject.get("pseudo").getAsString();
            String tentatives = scoreObject.get("tentatives").getAsString();
            String temps = scoreObject.get("temps").getAsString();
            // Affichez les données de chaque score
            System.out.println(pseudo+" : en "+tentatives+" tentatives et "+temps+" secondes.");
        }
    }
    public static void affichageScores(){
        String jsonContent = null;
        System.out.println(SEPARATION);
        try {
            jsonContent = Files.readString(Paths.get(JSON));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Gson gson = new Gson();
        // Analysez le contenu JSON en un objet Java
        JsonObject jsonObject = gson.fromJson(jsonContent, JsonObject.class);
        // Accédez aux données du JSON
        JsonArray scoresArray = jsonObject.getAsJsonArray("scores");
        int totalEntries = scoresArray.size();
        for (int i = 0; i < totalEntries; i++) {
            JsonObject scoreObject = scoresArray.get(i).getAsJsonObject();
            String pseudo = scoreObject.get("pseudo").getAsString();
            String tentatives = scoreObject.get("tentatives").getAsString();
            String temps = scoreObject.get("temps").getAsString();
            // Affichez les données de chaque score
            System.out.println(pseudo+" : en "+tentatives+" tentatives et "+temps+" secondes.");
        }
        System.out.println(SEPARATION);
        System.out.println(ALALIGNE);
    }
    public static void enregistrementScoreJson(String pseudo, int chrono, int tentatives) {
        String newEntryJson = "{\n" +
                "    \"pseudo\": \""+pseudo+"\",\n" +
                "    \"tentatives\": \""+tentatives+"\",\n" +
                "    \"temps\": \""+chrono+"\"\n" +
                "}";
        try {
            // Lire le contenu JSON existant depuis le fichier
            String jsonContent = Files.readString(Paths.get(JSON));
            // Créer un objet Gson
            Gson gson = new Gson();
            // Analyser le contenu JSON en une structure de données
            JsonObject jsonObject = gson.fromJson(jsonContent, JsonObject.class);
            // Accéder au tableau "scores" dans l'objet JSON
            JsonArray scoresArray = jsonObject.getAsJsonArray("scores");
            // Créer un objet JSON pour la nouvelle entrée
            JsonObject newEntry = gson.fromJson(newEntryJson, JsonObject.class);
            // Ajouter la nouvelle entrée au tableau "scores"
            scoresArray.add(newEntry);
            // Écrire la structure de données mise à jour dans le fichier JSON
            String updatedJson = gson.toJson(jsonObject);
            Files.write(Paths.get(JSON), updatedJson.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void rechercheLettre(String lettre) throws IOException {
        clearScreen();
        lettre = lettre.toLowerCase();
        Scanner myObj = new Scanner(System.in);
        String jsonContent = null;
        jsonContent = Files.readString(Paths.get(JSON));
        // Créez un objet Gson
        Gson gson = new Gson();
        // Analysez le contenu JSON en un objet Java
        JsonObject jsonObject = gson.fromJson(jsonContent, JsonObject.class);
        // Accédez au tableau "scores" dans le fichier score.json
        JsonArray scoresArray = jsonObject.getAsJsonArray("scores");
        int countPseudosStartingWithL = 0;
        // On parcours les éléments du tableau "scores"
        for (int i = 0; i < scoresArray.size(); i++) {
            JsonObject scoreObject = scoresArray.get(i).getAsJsonObject();
            String pseudo = scoreObject.get("pseudo").getAsString();
            pseudo = pseudo.toLowerCase();
            // Vérifiez si le pseudo commence par la ou les lettres entrées
            if (pseudo.startsWith(lettre)) {
                // Affichez les données de chaque score dont le pseudo commence par "L"
                pseudo = scoreObject.get("pseudo").getAsString();
                String tentatives = scoreObject.get("tentatives").getAsString();
                String temps = scoreObject.get("temps").getAsString();
                System.out.println(pseudo+" : en "+tentatives+" tentatives et "+temps+" secondes.");
                countPseudosStartingWithL++;
            }
        }
        if(countPseudosStartingWithL==0){
            System.out.println("Aucun pseudo commençant par "+lettre);
            System.out.println("Appuyez sur entrée pour retourner au menu principal");
            String menuPrincipal = myObj.nextLine();
            if(Objects.equals(menuPrincipal, "")){
                main(new String[]{""});
            }
        }else {
            System.out.println();
            System.out.println("Veuillez taper une autre lettre du pseudo recherché:");
            System.out.print(lettre);
            String autreLettre = lettre + myObj.nextLine();
            System.out.println(autreLettre);
            rechercheLettre(autreLettre);
        }
    }
}

