package org.ludo;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Scanner;

import static org.ludo.Main.*;
import static org.ludo.Scores.enregistrementScoreJson;
import static org.ludo.Fonctions.*;
public class Partie {
    public static String partie() {
        System.out.println("Veuillez indiquer votre pseudo:");
        Scanner myObj = new Scanner(System.in);
        String pseudo = myObj.nextLine();
        // On vérifie le pseudo
       while(!verificationPseudo(pseudo)){
           System.out.println("Vous ne pouvez pas prendre ce pseudo, veuillez indiquer un autre pseudo:");
           pseudo = myObj.nextLine();
       }
        System.out.println("Bonjour " + pseudo);
        // Initialisation du chrono
        long startChrono = System.currentTimeMillis();
        // Génération du chiffre à deviner
        int chiffre = nombre(MINI, MAXI);
        System.out.println();
        String question = "Choisir un nombre entre " + MINI + " et " + MAXI + " :";
        System.out.println(question);
        int reponse = myObj.nextInt();
        int tentative = 1;
        while (reponse != chiffre) {
            if (reponse < chiffre && reponse >= MINI) {
                clearScreen();
                System.out.println("C'est plus grand que " + reponse);
                System.out.println(question);
                reponse = myObj.nextInt();
                tentative++;
            } else if (reponse > chiffre && reponse < MAXI) {
                clearScreen();
                System.out.println("C'est plus petit que " + reponse);
                System.out.println(question);
                reponse = myObj.nextInt();
                tentative++;
            } else {
                clearScreen();
                System.out.println("Entrée incorrecte");
                System.out.println(question);
                reponse = myObj.nextInt();
                tentative++;
            }
        }
        clearScreen();
        long finChrono = System.currentTimeMillis();
        long chrono = (finChrono - startChrono) / 1000;
        enregistrementScoreTxt(pseudo, (int) chrono, tentative);
        enregistrementScoreJson(pseudo, (int) chrono, tentative);
        return "Vous avez gagné " + pseudo + ", le chiffre était bien le " + chiffre + " en " + tentative + " tentatives et " + chrono + " secondes.";
    }




    public static void enregistrementScoreTxt(String pseudo, int chrono, int tentatives) {
        /**
         * Fonction qui enregistre le score dans un fichier texte
         * S'il n'existe pas on le crée
         */
        try {
            File fichier = new File("./scores.txt");
            // Si le fichier scores.txt n'existe pas on le crée
            if (!fichier.exists() && (!fichier.createNewFile())) {
                    System.out.println("Création du fichier scores.json impossible");
            }
            Path path = Paths.get("./scores.txt");
            String retourLigne = System.getProperty("line.separator");
            String lignes = pseudo + " : " + chrono + " : " + tentatives + " " + retourLigne;
            Files.write(path, lignes.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
}

