package org.ludo;

import java.io.IOException;
import java.util.Scanner;

import static java.lang.System.*;
import static org.ludo.Fonctions.clearScreen;
import static org.ludo.Main.*;
import static org.ludo.Partie.partie;
import static org.ludo.Scores.*;

public class Menus {

    public static int menuPrincipal() {
        Scanner myobj = new Scanner(in);
        clearScreen();
        out.println(BARREMENU);
        out.println("|          MENU PRINCIPAL           |");
        out.println(BARREMENU);

        out.println(ALALIGNE);
        out.println("Scores des 10 dernières parties:");
        out.println(SEPARATION);
        affichageDernieresEntrees();
        out.println(SEPARATION);
        out.println(ALALIGNE);
        out.println("Choisir une option:");
        out.println("1- Scores");
        out.println("2- Jouer");
        int choix;
        choix = myobj.nextInt();
        return choix;
    }
    public static void menuScores() {
        clearScreen();
        out.println(BARREMENU);
        out.println("|               SCORES              |");
        out.println(BARREMENU);
        out.println();
        affichageScores();
    }
    public static String menuPartie() {
        clearScreen();
        out.println(BARREMENU);
        out.println("|              PARTIE               |");
        out.println(BARREMENU);
        return partie();
    }
    public static void menuRechercheJoueur(String lettre) throws IOException {
        clearScreen();
        out.println(BARREMENU);
        out.println("|          RECHERCHE JOUEUR         |");
        out.println(BARREMENU);
        out.println("\n");
        rechercheLettre(lettre);
    }
}
