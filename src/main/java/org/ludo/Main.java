package org.ludo;

import java.io.IOException;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.System.getProperty;
import static java.lang.System.in;
import static org.ludo.Menus.*;

public class Main {
    public static final String BARREMENU = "|###################################|";
    public static final String ALALIGNE = getProperty("line.separator");
    public static final int MINI = 1;
    public static final int MAXI = 1000;
    public static final String SEPARATION = "--------------------------------";
    public static void main(String[] strings) throws IOException {
        Scanner myobj = new Scanner(in);
        // Affiche le menu principal et redirige soit vers le menu partie, soit vers le menu score
        int choix = menuPrincipal();
        if(choix==1){
            // Affichage du menu Score
            menuScores();
            System.out.println("Entrez \"exit\" pour revenir au menu principal");
            System.out.println("Sinon entrez une lettre pour rechercher un joueur ");
            String retourScore = myobj.nextLine();
            // Si on entre "exit" on retourne au menu principal
            if(Objects.equals(retourScore.toLowerCase(), "exit")){
                main(new String[]{""});
            } else { // Sinon on va au menu de recherche de pseudo
                menuRechercheJoueur(retourScore);
            }
        }else{
            //Sinon on affiche le menu Partie
            String resultat = menuPartie();
            System.out.println(resultat);
            System.out.println("Appuyez sur entrée pour retourner au menu principal");
            String menuPrincipal = myobj.nextLine();
            if(Objects.equals(menuPrincipal, "")){
                // Retour au menu principal
                main(new String[]{""});
            }
        }
    }
}