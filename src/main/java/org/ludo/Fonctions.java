package org.ludo;

import static org.ludo.Main.ALALIGNE;
public class Fonctions {

    public static void clearScreen(){
        // Fonction qui efface le contenu de la console
        for(int i=0; i<50; i++) {
            System.out.print(ALALIGNE);
            System.out.flush();
        }
    }
    public static int nombre(int mini, int maxi) {
        /**
         * @param mini: int, chiffre minimum
         * @param maxi: int, chiffre maximum
         * @return int, un chiffre compris entre mini et maxi
         */
        return (int) (Math.random() * (mini + maxi));
    }
    public static boolean verificationPseudo(String pseudo) {
        // Vérifie si le pseudo contient le mot "exit" en minuscules ou majuscules
        if(pseudo.equalsIgnoreCase("exit")) {
            return false;
        }
        // Vérifie si le pseudo contient le caractère ":"
        return !pseudo.contains(":");
    }
}
